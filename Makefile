VENV=$(shell pwd)/venv/bin
PY3=$(VENV)/python3
FLAKE8=$(VENV)/flake8
BLACK=$(VENV)/black


run:
	@$(PY3) app.py

#lint:
	# stop the build if there are Python syntax errors or undefined names
	#$(FLAKE8) app.py --count --select=E9,F63,F7,F82 --show-source --statistics

format:
	$(BLACK) app.py