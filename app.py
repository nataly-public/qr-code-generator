import tkinter as tk
import pyqrcode
from PIL import ImageTk, Image


def create_code():
    qr = pyqrcode.create(input_str.get())
    print(qr)
    qr.png("output.png", scale=6)


    canvas.image = ImageTk.PhotoImage(
        Image.open("output.png").resize((200, 200), Image.LANCZOS)
    )

    canvas.create_image(0, 0, image=canvas.image, anchor="nw")


if __name__ == "__main__":
    window = tk.Tk()
    window.geometry("640x840")
    window.title("QR Code Generator")

    input_str = tk.StringVar(window)
    tk.Label(window, text="please, enter any text").pack()
    tk.Entry(
        window,
        textvariable=input_str,
        background="lightgreen",
        width=100,
        borderwidth=5,
    ).pack()
    tk.Label(window, text="get your qr code").pack()
    tk.Button(
        window,
        text="create",
        command=create_code,
        background="lightblue",
        width="100",
        height=5,
    ).pack()
    tk.Text(window, height=2).pack()

    canvas = tk.Canvas(width=200, height=200)
    canvas.pack(expand="yes", padx=(200, 200))

    window.mainloop()
